﻿using EBrokerApp.Core;
using Microsoft.EntityFrameworkCore;

namespace EBrokerApp.DataAccessLayer
{
	public class EBrokerAppDbContext:DbContext
	{
		public EBrokerAppDbContext(DbContextOptions options)
								: base(options)
		{

		}

		public virtual DbSet<User> Users { get; set; }
		public virtual DbSet<Equity> Equities { get; set; }
		public virtual DbSet<UserEquities> UserEquities { get; set; }

		protected override void OnModelCreating(ModelBuilder modelBuilder)
		{
			base.OnModelCreating(modelBuilder);
			modelBuilder.ApplyConfiguration(new EquityConfiguration());
		}
	}
}
