﻿using EBrokerApp.Core;

namespace EBrokerApp.DataAccessLayer
{
	public class UnitOfWork:IUnitOfWork
	{
		private readonly EBrokerAppDbContext _brokerAppDbContext;
		private IUserRepository _userRepository;
		private IRepository<Equity> _equityRepository;
		private IUserEquityRepository _userEquitiesRepository;

		public UnitOfWork(EBrokerAppDbContext dbContext)
		{
			_brokerAppDbContext = dbContext;
		}

		public IUserRepository Users => _userRepository ??= new UserRepository(_brokerAppDbContext);

		public IRepository<Equity> Equity => _equityRepository ??=new Repository<Equity>(_brokerAppDbContext);

		public IUserEquityRepository UserEquity => _userEquitiesRepository ??= new UserEquityRepository(_brokerAppDbContext);
		public int Complete()
		{
			return _brokerAppDbContext.SaveChanges();
		}

		public void Dispose()
		{
			_brokerAppDbContext.Dispose();
		}
	}
}
