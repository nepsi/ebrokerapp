﻿using EBrokerApp.Core;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;

namespace EBrokerApp.DataAccessLayer
{
	public class EquityConfiguration : IEntityTypeConfiguration<Equity>
	{
		private EntityTypeBuilder<Equity> _builder;

		public void Configure(EntityTypeBuilder<Equity> builder)
		{
			_builder = builder;
			DataSeed();
		}
		private void DataSeed()
		{
			this._builder.HasData(GetEquitiesData());
		}

		private List<Equity> GetEquitiesData()
		{
			return new List<Equity>()
			{
				new Equity()
				{
					Id=(int)EquityType.Stocks,
					Name=Enum.GetName(typeof(EquityType), (int)EquityType.Stocks),
					Price=10
				},
				new Equity()
				{
					Id=(int)EquityType.MutualFunds,
					Name=Enum.GetName(typeof(EquityType), (int)EquityType.MutualFunds),
					Price=20
				},
				new Equity()
				{
					Id=(int)EquityType.Futures,
					Name=Enum.GetName(typeof(EquityType), (int)EquityType.Futures),
					Price=25
				}
			};
		}
	}
}
