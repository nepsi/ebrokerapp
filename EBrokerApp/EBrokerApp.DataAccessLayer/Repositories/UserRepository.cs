﻿using EBrokerApp.Core;
using System.Linq;

namespace EBrokerApp.DataAccessLayer
{
	public class UserRepository : Repository<User>, IUserRepository
	{
		private readonly EBrokerAppDbContext _eBrokerAppDbContext;
		public UserRepository(EBrokerAppDbContext dbContext) : base(dbContext)
		{
			_eBrokerAppDbContext = dbContext;
		}

		public User GetUserByEmail(string email)
		{
			return _eBrokerAppDbContext.Users.FirstOrDefault(x => x.Email.Equals(email));
		}
	}
}
