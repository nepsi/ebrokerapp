﻿using EBrokerApp.Core;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace EBrokerApp.DataAccessLayer
{
	public class UserEquityRepository : Repository<UserEquities>, IUserEquityRepository
	{
		private readonly EBrokerAppDbContext _eBrokerAppDbContext;

		public UserEquityRepository(EBrokerAppDbContext dbContext) : base(dbContext)
		{
			_eBrokerAppDbContext = dbContext;
		}

		public UserEquities GetUserEquityByEquityTypeId(int userId, int equityTypeId)
		{
			return _eBrokerAppDbContext.UserEquities?.Where(x => x.EquityId.Equals(equityTypeId) && x.UserId.Equals(userId)).Include(x => x.Equity).FirstOrDefault();
		}
	}
}
