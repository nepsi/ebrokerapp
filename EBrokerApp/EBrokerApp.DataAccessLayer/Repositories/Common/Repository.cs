﻿using EBrokerApp.Core;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace EBrokerApp.DataAccessLayer
{
	public class Repository<TEntity> : IRepository<TEntity> where TEntity : class
	{
		protected readonly DbContext _dbContext;

		public Repository(DbContext dbContext)
		{
			_dbContext = dbContext;
		}
		public void Add(TEntity entity)
		{
			_dbContext.Set<TEntity>().Add(entity);
		}

		public void Update(TEntity entity)
		{
			_dbContext.Set<TEntity>().Update(entity);
		}

		public TEntity GetById(int id)
		{
			return _dbContext.Set<TEntity>().Find(id);
		}

		public IEnumerable<TEntity> GetAll()
		{
			return _dbContext.Set<TEntity>();
		}

		public void Remove(TEntity entity)
		{
			_dbContext.Set<TEntity>().Remove(entity);
		}
	}
}
