﻿using AutoMapper;
using EBrokerApp.Api;
using EBrokerApp.Api.Controllers;
using EBrokerApp.Core;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Xunit;

namespace EBrokerApp.UnitTest
{
	public class ControllerUnitTest
	{

		private readonly Mock<IUserService> _mockUserService;
		private readonly UserEquityController _userEquityController; 
		public ControllerUnitTest()
		{
			MapperConfiguration config = new MapperConfiguration(cfg =>
			{
				cfg.AddProfile(new EntityMapping());
			});

			_mockUserService = new Mock<IUserService>();
			_userEquityController = new UserEquityController(config.CreateMapper(), _mockUserService.Object);
		}

		[Fact]
		public void GetOkResultWhenCalledBuyEquity()
		{
			this._mockUserService.Setup(s => s.BuyEquities(It.IsAny<UserEquities>())).Returns(Constants.Success);

			var result = _userEquityController.BuyEquity(new Core.DTO.UserEquityDto() { Units = 10, EquityTypeId = 1, Email = "test@gmail.com" }) as ObjectResult;

			Assert.IsType<OkObjectResult>(result);

			var response = Assert.IsType<string>(result.Value);
			Assert.Equal(Constants.Success, response);
		}

		[Fact]
		public void GetBadObjectResultWhenCalledBuyEquityWithNullValue()
		{
			this._mockUserService.Setup(s => s.BuyEquities(It.IsAny<UserEquities>())).Returns(Constants.Success);

			var result = _userEquityController.BuyEquity(null) as ObjectResult;

			Assert.IsType<BadRequestObjectResult>(result);

			var response = Assert.IsType<string>(result.Value);
			Assert.Equal(Constants.EquityDetailsNotProvided, response);
		}

		[Fact]
		public void GetBadObjectResultWhenCalledBuyEquity()
		{
			this._mockUserService.Setup(s => s.BuyEquities(It.IsAny<UserEquities>())).Returns(Constants.Failed);

			var result = _userEquityController.BuyEquity(new Core.DTO.UserEquityDto() { Units = 10, EquityTypeId = 1, Email = "test@gmail.com" }) as ObjectResult;

			Assert.IsType<BadRequestObjectResult>(result);

			var response = Assert.IsType<string>(result.Value);
			Assert.Equal(Constants.Failed, response);
		}

		[Fact]
		public void GetOkResultWhenCalledSellEquity()
		{
			this._mockUserService.Setup(s => s.SellEquities(It.IsAny<UserEquities>())).Returns(Constants.Success);

			var result = _userEquityController.SellEquity(new Core.DTO.UserEquityDto() { Units = 10, EquityTypeId = 1, Email = "test@gmail.com" }) as ObjectResult;

			Assert.IsType<OkObjectResult>(result);

			var response = Assert.IsType<string>(result.Value);
			Assert.Equal(Constants.Success, response);
		}

		[Fact]
		public void GetBadObjectResultWhenCalledSellEquityWithNullValue()
		{
			this._mockUserService.Setup(s => s.SellEquities(It.IsAny<UserEquities>())).Returns(Constants.Success);

			var result = _userEquityController.SellEquity(null) as ObjectResult;

			Assert.IsType<BadRequestObjectResult>(result);

			var response = Assert.IsType<string>(result.Value);
			Assert.Equal(Constants.EquityDetailsNotProvided, response);
		}

		[Fact]
		public void GetBadObjectResultWhenCalledSellEquity()
		{
			this._mockUserService.Setup(s => s.SellEquities(It.IsAny<UserEquities>())).Returns(Constants.Failed);

			var result = _userEquityController.SellEquity(new Core.DTO.UserEquityDto() { Units = 10, EquityTypeId = 1, Email = "test@gmail.com" }) as ObjectResult;

			Assert.IsType<BadRequestObjectResult>(result);

			var response = Assert.IsType<string>(result.Value);
			Assert.Equal(Constants.Failed, response);
		}


		[Fact]
		public void GetOkResultWhenCalledAddFund()
		{
			this._mockUserService.Setup(s => s.AddFund(It.IsAny<User>())).Returns(true);

			var result = _userEquityController.AddUserFunds(new Core.DTO.UserDto() {  Email = "test@gmail.com", Fund=10000 }) as ObjectResult;

			Assert.IsType<OkObjectResult>(result);

			var response = Assert.IsType<bool>(result.Value);
			Assert.True(response);
		}

		[Fact]
		public void GetBadObjectResultWhenCalledAddFundWithNullValue()
		{
			this._mockUserService.Setup(s => s.AddFund(It.IsAny<User>())).Returns(true);

			var result = _userEquityController.AddUserFunds(null) as ObjectResult;

			Assert.IsType<BadRequestObjectResult>(result);

			var response = Assert.IsType<string>(result.Value);
			Assert.Equal(Constants.DetailsNotProvided, response);
		}

		[Fact]
		public void GetBadObjectResultWhenCalledAddFund()
		{
			this._mockUserService.Setup(s => s.AddFund(It.IsAny<User>())).Returns(false);

			var result = _userEquityController.AddUserFunds(new Core.DTO.UserDto() { Email = "test@gmail.com", Fund = 10000 }) as ObjectResult;

			Assert.IsType<BadRequestObjectResult>(result);

			var response = Assert.IsType<bool>(result.Value);
			Assert.False(response);
		}

	}
}
