using EBrokerApp.BusinessLayer;
using EBrokerApp.Core;
using Moq;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace EBrokerApp.UnitTest
{
	public class UserServiceTest
	{
		private readonly Mock<IUnitOfWork> _mockUnitOfWork;
		private IUserService _userService;
		private readonly Mock<IWrapper> _wrapper;
		public UserServiceTest()
		{
			_mockUnitOfWork = new Mock<IUnitOfWork>();
			_wrapper = new Mock<IWrapper>();
			string s = Constants.Success;
			_wrapper.Setup(x => x.IsValidTradeDateTime(out s)).Returns(true);
			_userService = new UserService(_mockUnitOfWork.Object, _wrapper.Object);
		}

		[Fact]
		public void GetSuccessMessageWhenBuyEquitiesCalled()
		{
			List<Equity> equities = TestDataHelper.GetEquitiesData();
			User user = new User() { Balance = 100, Id = 1, Email = "test@gmail.com" };
			this._mockUnitOfWork.Setup(x => x.Equity.GetById(It.IsAny<int>())).Returns<int>(x =>
			  {
				  return equities.FirstOrDefault(eq => eq.Id == x);
			  });

			this._mockUnitOfWork.Setup(x => x.Users.GetUserByEmail(It.IsAny<string>())).Returns(user);

			this._mockUnitOfWork.Setup(x => x.UserEquity.Add(It.IsAny<UserEquities>()));
			UserEquities userEquities = new UserEquities()
			{
				EquityId = 1,
				Units = 10,
				User = user
			};

			string result = _userService.BuyEquities(userEquities);
			Assert.IsType<string>(result);
			Assert.Equal(Constants.Success, result);
		}

		[Fact]
		public void GetFailedMessageWhenBuyEquitiesCalled()
		{
			List<Equity> equities = TestDataHelper.GetEquitiesData();
			User user = new User() { Balance = 10, Id = 1, Email = "test@gmail.com" };
			this._mockUnitOfWork.Setup(x => x.Equity.GetById(It.IsAny<int>())).Returns<int>(x =>
			{
				return equities.FirstOrDefault(eq => eq.Id == x);
			});

			this._mockUnitOfWork.Setup(x => x.Users.GetUserByEmail(It.IsAny<string>())).Returns(user);

			this._mockUnitOfWork.Setup(x => x.UserEquity.Add(It.IsAny<UserEquities>()));
			UserEquities userEquities = new UserEquities()
			{
				EquityId = 1,
				Units = 10,
				User = user
			};

			string result = _userService.BuyEquities(userEquities);
			Assert.IsType<string>(result);
			Assert.Equal(Constants.UserDoesNotHaveBalance, result);
		}

		[Fact]
		public void GetSuccessMessageWhenUserAlreadyHasEquityAndBuyEquitiesCalled()
		{
			List<Equity> equities = TestDataHelper.GetEquitiesData();
			User user = new User() { Balance = 100, Id = 1, Email = "test@gmail.com",
				Equities = new List<UserEquities>() { new UserEquities() { EquityId = 1, Units = 5 } }
			};
			this._mockUnitOfWork.Setup(x => x.Equity.GetById(It.IsAny<int>())).Returns<int>(x =>
			{
				return equities.FirstOrDefault(eq => eq.Id == x);
			});

			this._mockUnitOfWork.Setup(x => x.Users.GetUserByEmail(It.IsAny<string>())).Returns(user);

			this._mockUnitOfWork.Setup(x => x.UserEquity.Update(It.IsAny<UserEquities>()));
			UserEquities userEquities = new UserEquities()
			{
				EquityId = 1,
				Units = 10,
				User = user
			};

			string result = _userService.BuyEquities(userEquities);
			Assert.IsType<string>(result);
			Assert.Equal(Constants.Success, result);
		}
		[Fact]
		public void GetSuccessMessageWhenSellEquitiesCalled()
		{
			List<Equity> equities = TestDataHelper.GetEquitiesData();
			User user = new User() { Balance = 100, Id = 1, Email = "test@gmail.com", Equities = new List<UserEquities>() { new UserEquities() { EquityId = 1, Units = 10, UserId = 1 } } };

			this._mockUnitOfWork.Setup(x => x.Equity.GetById(It.IsAny<int>())).Returns<int>(x =>
			{
				return equities.FirstOrDefault(eq => eq.Id == x);
			});

			this._mockUnitOfWork.Setup(x => x.Users.GetById(It.IsAny<int>())).Returns(user);

			this._mockUnitOfWork.Setup(x => x.UserEquity.Update(It.IsAny<UserEquities>()));

			this._mockUnitOfWork.Setup(x => x.UserEquity.GetUserEquityByEquityTypeId(It.IsAny<int>(), It.IsAny<int>())).Returns<int, int>((x, y) =>
			{
				return user.Equities.FirstOrDefault(eq => eq.UserId == x && eq.EquityId==y);
			});
			UserEquities userEquities = new UserEquities()
			{
				EquityId = 1,
				Units = 5,
				User = user,
				UserId = user.Id
			};

			string result = _userService.SellEquities(userEquities);
			Assert.IsType<string>(result);
			Assert.Equal(Constants.Success, result);
		}

		[Fact]
		public void GetUserDoesNotHaveEquitiesMessageWhenSellEquitiesCalled()
		{
			List<Equity> equities = TestDataHelper.GetEquitiesData();
			User user = new User() { Balance = 100, Id = 1, Email = "test@gmail.com" };

			this._mockUnitOfWork.Setup(x => x.Equity.GetById(It.IsAny<int>())).Returns<int>(x =>
			{
				return equities.FirstOrDefault(eq => eq.Id == x);
			});

			this._mockUnitOfWork.Setup(x => x.Users.GetById(It.IsAny<int>())).Returns(user);

			this._mockUnitOfWork.Setup(x => x.UserEquity.Update(It.IsAny<UserEquities>()));
			UserEquities userEquities = new UserEquities()
			{
				EquityId = 1,
				Units = 5,
				User = user,
				UserId = user.Id
			};

			string result = _userService.SellEquities(userEquities);
			Assert.IsType<string>(result);
			Assert.Equal(Constants.UserDoesNotHaveEquities, result);
		}

		[Fact]
		public void GetUserDoesNotHaveBalanceForBrokerageMessageWhenSellEquitiesCalled()
		{
			List<Equity> equities = TestDataHelper.GetEquitiesData();
			User user = new User() { Balance = 5, Id = 1, Email = "test@gmail.com", Equities = new List<UserEquities>() { new UserEquities() { EquityId = 2, Units = 10, UserId = 1 } } };

			this._mockUnitOfWork.Setup(x => x.Equity.GetById(It.IsAny<int>())).Returns<int>(x =>
			{
				return equities.FirstOrDefault(eq => eq.Id == x);
			});

			this._mockUnitOfWork.Setup(x => x.Users.GetById(It.IsAny<int>())).Returns(user);

			this._mockUnitOfWork.Setup(x => x.UserEquity.Update(It.IsAny<UserEquities>()));

			this._mockUnitOfWork.Setup(x => x.UserEquity.GetUserEquityByEquityTypeId(It.IsAny<int>(), It.IsAny<int>())).Returns<int, int>((x, y) =>
			{
				return user.Equities.FirstOrDefault(eq => eq.UserId == x && eq.EquityId == y);
			});
			UserEquities userEquities = new UserEquities()
			{
				EquityId = 2,
				Units = 10,
				User = user,
				UserId = user.Id
			};

			string result = _userService.SellEquities(userEquities);
			Assert.IsType<string>(result);
			Assert.Equal(Constants.UserDoesNotHaveBalance, result);
		}

		[Fact]
		public void CheckIfFundsAdded()
		{
			User user = new User() { Balance = 100, Id = 1, Email = "test@gmail.com" };
			this._mockUnitOfWork.Setup(x => x.Users.GetUserByEmail(It.IsAny<string>())).Returns(user);

			User updatedData = new User() { Balance = 10000, Email = "test@gmail.com" };
			bool result = _userService.AddFund(updatedData);
			Assert.True(result);
		}

		[Fact]
		public void CheckIfFundsAddedAboveMaxAmount()
		{
			User user = new User() { Balance = 100, Id = 1, Email = "test@gmail.com" };
			this._mockUnitOfWork.Setup(x => x.Users.GetUserByEmail(It.IsAny<string>())).Returns(user);
			User updatedData = new User() { Balance = 1000000, Email = "test@gmail.com" };
			bool result = _userService.AddFund(updatedData);

			Assert.True(result);
			_mockUnitOfWork.VerifyAll();
		}

		[Fact]
		public void GetSuccessMessageWhenBuyEquitiesCalledOnWeekDay()
		{
			var wrapper = new UtilitiesWrapper();
			wrapper.CurrentDate = new System.DateTime(2021, 12, 21, 10, 0, 0);
			_userService = new UserService(_mockUnitOfWork.Object, wrapper);

			List<Equity> equities = TestDataHelper.GetEquitiesData();
			User user = new User() { Balance = 100, Id = 1, Email = "test@gmail.com" };
			this._mockUnitOfWork.Setup(x => x.Equity.GetById(It.IsAny<int>())).Returns<int>(x =>
			{
				return equities.FirstOrDefault(eq => eq.Id == x);
			});

			this._mockUnitOfWork.Setup(x => x.Users.GetUserByEmail(It.IsAny<string>())).Returns(user);

			this._mockUnitOfWork.Setup(x => x.UserEquity.Add(It.IsAny<UserEquities>()));
			UserEquities userEquities = new UserEquities()
			{
				EquityId = 1,
				Units = 10,
				User = user
			};

			string result = _userService.BuyEquities(userEquities);
			Assert.IsType<string>(result);
			Assert.Equal(Constants.Success, result);
		}

		[Fact]
		public void GetErrorMessageWhenBuyEquitiesCalledOnWeekend()
		{
			var wrapper = new UtilitiesWrapper();
			wrapper.CurrentDate = new System.DateTime(2021, 12, 18, 10, 0, 0);
			_userService = new UserService(_mockUnitOfWork.Object, wrapper);

			List<Equity> equities = TestDataHelper.GetEquitiesData();
			User user = new User() { Balance = 100, Id = 1, Email = "test@gmail.com" };
			this._mockUnitOfWork.Setup(x => x.Equity.GetById(It.IsAny<int>())).Returns<int>(x =>
			{
				return equities.FirstOrDefault(eq => eq.Id == x);
			});

			this._mockUnitOfWork.Setup(x => x.Users.GetUserByEmail(It.IsAny<string>())).Returns(user);

			this._mockUnitOfWork.Setup(x => x.UserEquity.Add(It.IsAny<UserEquities>()));
			UserEquities userEquities = new UserEquities()
			{
				EquityId = 1,
				Units = 10,
				User = user
			};

			string result = _userService.BuyEquities(userEquities);
			Assert.IsType<string>(result);
			Assert.Equal(Constants.InvalidDay, result);
		}

		[Fact]
		public void GetErrorMessageWhenBuyEquitiesCalledOutsideNineAmToThreePm()
		{
			var wrapper = new UtilitiesWrapper();
			wrapper.CurrentDate = new System.DateTime(2021, 12, 21, 15,30, 0);
			_userService = new UserService(_mockUnitOfWork.Object, wrapper);

			List<Equity> equities = TestDataHelper.GetEquitiesData();
			User user = new User() { Balance = 100, Id = 1, Email = "test@gmail.com" };
			this._mockUnitOfWork.Setup(x => x.Equity.GetById(It.IsAny<int>())).Returns<int>(x =>
			{
				return equities.FirstOrDefault(eq => eq.Id == x);
			});

			this._mockUnitOfWork.Setup(x => x.Users.GetUserByEmail(It.IsAny<string>())).Returns(user);

			this._mockUnitOfWork.Setup(x => x.UserEquity.Add(It.IsAny<UserEquities>()));
			UserEquities userEquities = new UserEquities()
			{
				EquityId = 1,
				Units = 10,
				User = user
			};

			string result = _userService.BuyEquities(userEquities);
			Assert.IsType<string>(result);
			Assert.Equal(Constants.InvalidTime, result);
		}

		[Fact]
		public void CheckIfFundsHaveMinAmount()
		{
			User user = new User() { Balance = 100, Id = 1, Email = "test@gmail.com" };
			this._mockUnitOfWork.Setup(x => x.Users.GetUserByEmail(It.IsAny<string>())).Returns(user);

			User updatedData = new User() { Balance = -23, Email = "test@gmail.com" };
			bool result = _userService.AddFund(updatedData);
			Assert.False(result);
		}
	}
}
