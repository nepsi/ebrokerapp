﻿using EBrokerApp.Core;
using EBrokerApp.DataAccessLayer;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace EBrokerApp.UnitTest
{
	public class RepositoryUnitTest: IDisposable
	{
		private DbContextOptions Options;
		public RepositoryUnitTest()
		{
			Options = new DbContextOptionsBuilder<EBrokerAppDbContext>().UseInMemoryDatabase(databaseName: "EquityTraderDatabase").Options;
			using (var context = new EBrokerAppDbContext(Options))
			{
				context.Database.EnsureDeleted();
				context.Equities.AddRange(TestDataHelper.GetEquitiesData());
				context.Users.Add(new User() { Balance = 100, Id = 1, Email = "test@gmail.com" });
				context.UserEquities.Add(new UserEquities() { UserId = 1, EquityId = 1, Units = 10 });
				context.SaveChanges();
			}
		}

		[Fact]
		public void CheckIfEquityFetchedCorrectly_True()
		{
			using (var context = new EBrokerAppDbContext(Options))
			{
				IUnitOfWork unitOfWork = new UnitOfWork(context);
				Equity equity= unitOfWork.Equity.GetById(1);
				Assert.NotNull(equity);
				Assert.Equal("Stocks", equity.Name);
			}
		}

		[Fact]
		public void CheckIfUserEquityFetchedCorrectly_True()
		{
			using (var context = new EBrokerAppDbContext(Options))
			{
				IUnitOfWork unitOfWork = new UnitOfWork(context);
				UserEquities equity = unitOfWork.UserEquity.GetUserEquityByEquityTypeId(1, 1);
				Assert.NotNull(equity);
				Assert.NotEqual(0, equity.Id);
				Assert.NotNull(equity.Equity);
				Assert.Equal(10, equity.Units);
			}
		}

		[Fact]
		public void CheckIfUserEquityAddedCorrectly_True()
		{
			UserEquities equity = new UserEquities() { EquityId = 2, UserId = 1, Units = 5 };

			using (var context = new EBrokerAppDbContext(Options))
			{
				int count = context.UserEquities.Count(x => x.UserId == 1);
				Assert.Equal(1, count);

				IUnitOfWork unitOfWork = new UnitOfWork(context);
				unitOfWork.UserEquity.Add(equity);
				unitOfWork.Complete();

				count = context.UserEquities.Count(x => x.UserId == 1);
				Assert.Equal(2, count);
			}
		}

		[Fact]
		public void CheckIfUserEquityUpdatedCorrectly_True()
		{
			using (var context = new EBrokerAppDbContext(Options))
			{
				var equity = context.UserEquities.FirstOrDefault(x => x.UserId == 1);
				Assert.Equal(10, equity.Units);

				IUnitOfWork unitOfWork = new UnitOfWork(context);
				equity.Units = 15;
				unitOfWork.UserEquity.Update(equity);
				unitOfWork.Complete();

				var newEquity = context.UserEquities.FirstOrDefault(x => x.UserId == 1);
				Assert.Equal(15, newEquity.Units);
			}
		}

		[Fact]
		public void CheckIfUserUpdatedCorrectly_True()
		{
			using (var context = new EBrokerAppDbContext(Options))
			{
				var user = context.Users.FirstOrDefault(x => x.Id == 1);
				Assert.NotNull(user);
				Assert.Equal(100, user.Balance);

				IUnitOfWork unitOfWork = new UnitOfWork(context);
				user.Balance = 300;
				unitOfWork.Users.Update(user);
				unitOfWork.Complete();
				user = context.Users.FirstOrDefault(x => x.Id == 1);
				Assert.Equal(300, user.Balance);
			}
		}

		[Fact]
		public void CheckIfUserFetchedCorrectly_True()
		{
			using (var context = new EBrokerAppDbContext(Options))
			{
				IUnitOfWork unitOfWork = new UnitOfWork(context);
				var user = unitOfWork.Users.GetUserByEmail("test@gmail.com");
				Assert.NotNull(user);
			}
		}

		[Fact]
		public void CheckIfAllEquityFetchedCorrectly_True()
		{
			using (var context = new EBrokerAppDbContext(Options))
			{
				IUnitOfWork unitOfWork = new UnitOfWork(context);
				IEnumerable<Equity> equity = unitOfWork.Equity.GetAll();
				Assert.Equal(3, equity.Count());
			}
		}

		[Fact]
		public void CheckIfUserRemovedCorrectly_True()
		{
			using (var context = new EBrokerAppDbContext(Options))
			{
				IUnitOfWork unitOfWork = new UnitOfWork(context);
				var user = unitOfWork.Users.GetUserByEmail("test@gmail.com");
				Assert.NotNull(user);

				unitOfWork.Users.Remove(user);
				unitOfWork.Complete();
				var removedUser = context.Users.FirstOrDefault(x => x.Email.Equals("test@gmail.com"));
				Assert.Null(removedUser);
			}
		}
		public void Dispose()
		{
			using (var context = new EBrokerAppDbContext(Options))
				context.Dispose();
		}
	}
}
