﻿using EBrokerApp.Core;
using System;
using System.Collections.Generic;
using System.Text;

namespace EBrokerApp.UnitTest
{
	public static class TestDataHelper
	{
		public static List<Equity> GetEquitiesData()
		{
			return new List<Equity>()
			{
				new Equity()
				{
					Id=(int)EquityType.Stocks,
					Name=Enum.GetName(typeof(EquityType), (int)EquityType.Stocks),
					Price=10
				},
				new Equity()
				{
					Id=(int)EquityType.MutualFunds,
					Name=Enum.GetName(typeof(EquityType), (int)EquityType.MutualFunds),
					Price=1
				},
				new Equity()
				{
					Id=(int)EquityType.Futures,
					Name=Enum.GetName(typeof(EquityType), (int)EquityType.Futures),
					Price=25
				}
			};
		}
	}
}
