﻿using AutoMapper;
using EBrokerApp.Core;
using EBrokerApp.Core.DTO;

namespace EBrokerApp.Api
{
	public class EntityMapping : Profile
	{
		public EntityMapping()
		{
			UserEquityMapping();
			UserMapping();
		}

		private void UserEquityMapping()
		{
			CreateMap<UserEquities, UserEquityDto>()
							.ForMember(t => t.Units, x => x.MapFrom(x => x.Units))
							.ForMember(x => x.EquityTypeId, x => x.MapFrom(x => x.EquityId))
							.ForMember(x => x.Email, x => x.MapFrom(x => x.User.Email)).ReverseMap();
		}
		private void UserMapping()
		{
			CreateMap<User, UserDto>()
							.ForMember(t => t.Email, x => x.MapFrom(x => x.Email))
							.ForMember(x => x.Fund, x => x.MapFrom(x => x.Balance)).ReverseMap();
		}

	}
}
