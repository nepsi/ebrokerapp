﻿using AutoMapper;
using EBrokerApp.Core;
using EBrokerApp.Core.DTO;
using Microsoft.AspNetCore.Mvc;

namespace EBrokerApp.Api.Controllers
{
	public class UserEquityController : ControllerBase
	{
		private readonly IUserService _userService;
		private readonly IMapper _mapper;
		public UserEquityController(IMapper mapper, IUserService userService)
		{
			_mapper = mapper;
			_userService = userService;
		}

		[HttpPost]
		public IActionResult BuyEquity(UserEquityDto userEquityDto)
		{
			UserEquities equities = this._mapper.Map<UserEquities>(userEquityDto);
			if (equities != null)
			{
				string result = _userService.BuyEquities(equities);
				return result switch
				{
					Constants.Success => Ok(result),
					_ => BadRequest(result)
				};
			}
			else
			{
				return BadRequest(Constants.EquityDetailsNotProvided);
			}
		}

		[HttpPost]
		public IActionResult SellEquity(UserEquityDto userEquityDto)
		{
			UserEquities equities = this._mapper.Map<UserEquities>(userEquityDto);
			if (equities != null)
			{
				string result = _userService.SellEquities(equities);
				return result switch
				{
					Constants.Success => Ok(result),
					_ => BadRequest(result)
				};
			}
			else
			{
				return BadRequest("Equity details are not provided");
			}
		}

		[HttpPut]
		public IActionResult AddUserFunds(UserDto userDto)
		{
			User user = this._mapper.Map<User>(userDto);
			if (user != null)
			{
				bool result = _userService.AddFund(user);
				return result switch
				{
					true => Ok(result),
					_ => BadRequest(result)
				};
			}
			else
			{
				return BadRequest(Constants.DetailsNotProvided);
			}
		}
	}
}
