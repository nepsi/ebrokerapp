﻿using System.Collections.Generic;

namespace EBrokerApp.Core
{
	public class User
	{
		public int Id { get; set; }
		public decimal Balance { get; set; }
		public string Email { get; set; }
		public virtual List<UserEquities> Equities { get; set; }
	}
}
