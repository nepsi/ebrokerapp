﻿namespace EBrokerApp.Core
{
	public class Equity
	{
		public int Id { get; set; }
		public string Name { get; set; }
		public decimal Price { get; set; }
	}
}
