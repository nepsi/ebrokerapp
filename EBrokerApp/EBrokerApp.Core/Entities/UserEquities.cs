﻿namespace EBrokerApp.Core
{
	public class UserEquities
	{
		public int Id { get; set; }
		public int EquityId { get; set; }
		public virtual Equity Equity { get; set; }
		public int Units { get; set; }
		public int UserId { get; set; }
		public virtual User User { get; set; }
	}
}
