﻿namespace EBrokerApp.Core
{
	public enum EquityType
	{
		Stocks=1,
		MutualFunds=2,
		Futures=3
	}
}
