﻿namespace EBrokerApp.Core.DTO
{
	public class UserDto
	{
		public string Email { get; set; }
		public decimal Fund { get; set; }
	}
}
