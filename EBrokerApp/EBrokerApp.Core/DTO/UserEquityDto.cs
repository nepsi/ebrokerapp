﻿namespace EBrokerApp.Core.DTO
{
	public class UserEquityDto
	{
		public int EquityTypeId { get; set; }
		public int Units { get; set; }

		public string Email { get; set; }
	}
}
