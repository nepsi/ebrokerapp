﻿namespace EBrokerApp.Core
{
	public interface IWrapper
	{
		bool IsValidTradeDateTime(out string validationMsg);
	}
}
