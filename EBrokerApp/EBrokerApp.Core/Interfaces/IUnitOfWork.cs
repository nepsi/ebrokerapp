﻿namespace EBrokerApp.Core
{
	public interface IUnitOfWork
	{
		IUserRepository Users { get; }
		IRepository<Equity> Equity { get; }
		IUserEquityRepository UserEquity { get; }
		int Complete();
		void Dispose();
	}
}
