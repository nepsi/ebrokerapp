﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace EBrokerApp.Core
{
	public interface IRepository<TEntity> where TEntity : class
	{
		TEntity GetById(int id);
		IEnumerable<TEntity> GetAll();
		void Add(TEntity entity);
		void Remove(TEntity entity);
		void Update(TEntity entity);
	}
}
