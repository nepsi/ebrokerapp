﻿namespace EBrokerApp.Core
{
	public interface IUserRepository: IRepository<User>
	{
		User GetUserByEmail(string email);
	}
}
