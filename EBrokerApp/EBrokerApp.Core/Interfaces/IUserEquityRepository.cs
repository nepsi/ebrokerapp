﻿using System.Collections.Generic;

namespace EBrokerApp.Core
{
	public interface IUserEquityRepository : IRepository<UserEquities>
	{
		UserEquities GetUserEquityByEquityTypeId(int userId, int equityTypeId);
	}
}
