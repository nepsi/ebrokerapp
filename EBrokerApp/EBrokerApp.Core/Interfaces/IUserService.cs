﻿namespace EBrokerApp.Core
{
	public interface IUserService
	{
		string BuyEquities(UserEquities equities);
		string SellEquities(UserEquities equities);
		bool AddFund(User updatedUser);
	}
}
