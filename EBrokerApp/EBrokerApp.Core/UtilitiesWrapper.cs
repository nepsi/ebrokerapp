﻿using System;

namespace EBrokerApp.Core
{
	public class UtilitiesWrapper : IWrapper
	{
		public DateTime CurrentDate { get; set; } = DateTime.Now;
		public bool IsValidTradeDateTime(out string validationMsg)
		{
			return Utilities.IsValidTradeDateTime(CurrentDate, out validationMsg);
		}
	}
}
