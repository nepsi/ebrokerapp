﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EBrokerApp.Core
{
	public static class Constants
	{
		public const string Success = "Success";
		public const string Failed = "Failed";
		public const string InvalidTime = "Equity can be bought from 9am to 3pm";
		public const string InvalidDay = "Equity can be bought from Monday to Friday";
		public const string UserDoesNotHaveBalance = "User does not have enough balance";
		public const string UserDoesNotHaveEquities = "User does not have enough equities.";
		public const string EquityDetailsNotProvided = "Equity details are not provided";
		public const string DetailsNotProvided = "Data not provided";
	}
}
