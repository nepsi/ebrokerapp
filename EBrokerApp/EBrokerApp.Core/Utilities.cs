﻿using System;

namespace EBrokerApp.Core
{
	public static class Utilities
	{
		public static bool IsValidTradeDateTime(DateTime tradeTime, out string validationMsg)
		{
			validationMsg = string.Empty;
			TimeSpan start = new TimeSpan(9, 0, 0);
			TimeSpan end = new TimeSpan(15, 0, 0);
			TimeSpan currentTime = tradeTime.TimeOfDay;
			DayOfWeek day = tradeTime.DayOfWeek;
			if (!(currentTime >= start && currentTime <= end))
				validationMsg = Constants.InvalidTime;
			else if (day == DayOfWeek.Saturday || day == DayOfWeek.Sunday)
				validationMsg = Constants.InvalidDay;

			if (string.IsNullOrWhiteSpace(validationMsg))
				return true;
			return false;
		}
	}
}
