﻿using EBrokerApp.Core;
using System;
using System.Linq;

namespace EBrokerApp.BusinessLayer
{
	public class UserService : IUserService
	{
		private readonly IUnitOfWork _unitOfWork;
		private readonly IWrapper _wrapper;
		public UserService(IUnitOfWork unitOfWork, IWrapper wrapper)
		{
			_unitOfWork = unitOfWork;
			_wrapper = wrapper;
		}

		public string BuyEquities(UserEquities equities)
		{
			string validationMsg = string.Empty;
			if (_wrapper.IsValidTradeDateTime(out validationMsg))
			{
				// check user balance.
				if (ValidateUserBalance(equities, out decimal amount, out User user))
				{
					UserEquities equity = user.Equities?.FirstOrDefault(x => x.EquityId.Equals(equities.EquityId));
					if (equity != null)
					{
						equity.Units += equities.Units;
						_unitOfWork.UserEquity.Update(equity);
					}
					else
					{
						equity = new UserEquities
						{
							UserId = user.Id,
							EquityId = (int)equities.EquityId,
							Units = equities.Units
						};
						_unitOfWork.UserEquity.Add(equity);
					}

					user.Balance = user.Balance - amount;
					_unitOfWork.Users.Update(user);
					return _unitOfWork.Complete() >= 0 ? Constants.Success : Constants.Failed;
				}
				else
				{
					validationMsg = Constants.UserDoesNotHaveBalance;
				}
			}

			return validationMsg;
		}

		private bool ValidateUserBalance(UserEquities equities, out decimal amount, out User user)
		{
			Equity equityToPurchase = _unitOfWork.Equity.GetById(equities.EquityId);
			amount = equityToPurchase.Price * equities.Units;
			user = _unitOfWork.Users.GetUserByEmail(equities.User.Email);
			return user.Balance >= amount;
		}

		public string SellEquities(UserEquities equities)
		{
			string validationMsg = string.Empty;
			if (_wrapper.IsValidTradeDateTime(out validationMsg))
			{
				User user = _unitOfWork.Users.GetById(equities.UserId);
				Equity equityToSell = _unitOfWork.Equity.GetById(equities.EquityId);
				UserEquities userEquities = _unitOfWork.UserEquity.GetUserEquityByEquityTypeId(equities.UserId, equities.EquityId);
				if (userEquities != null && userEquities.Units >= equities.Units)
				{
					userEquities.Units -= equities.Units;
					double amount = (double)(equityToSell.Price * equities.Units);
					double brokerage = Math.Max(0.05 * amount, 20.0);
					if ((double)user.Balance + amount >= brokerage)
					{
						user.Balance = user.Balance + (decimal)(amount - brokerage);
						_unitOfWork.UserEquity.Update(userEquities);
						_unitOfWork.Users.Update(user);
						return _unitOfWork.Complete() >= 0 ? Constants.Success : Constants.Failed;
					}
					else
					{
						return Constants.UserDoesNotHaveBalance;
					}
				}
				else
				{
					validationMsg = Constants.UserDoesNotHaveEquities;
				}
			}

			return validationMsg;
		}

		public bool AddFund(User updatedUser)
		{
			decimal charges = 0;
			if (updatedUser.Balance <= 0)
				return false;

			if (updatedUser.Balance >= 100000)
			{
				charges = (updatedUser.Balance - 100000) * (decimal)0.05;
			}

			User user = _unitOfWork.Users.GetUserByEmail(updatedUser.Email);
			user.Balance += (decimal)(updatedUser.Balance - charges);
			_unitOfWork.Users.Update(user);
			return _unitOfWork.Complete() >= 0;
		}
	}
}
